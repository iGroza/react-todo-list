import React from 'react'
import styled from 'styled-components'

interface ISeparatorProps {
    width: string
}

const Separator = styled.div<ISeparatorProps>`
    height: 100%;
    width: ${props => props.width}
`

export default Separator