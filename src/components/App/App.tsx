import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from "styled-components";
import { todoListAction } from '../../store/application/todoList/action';
import Button from '../buttons/Button';
import ButtonArea from '../buttons/ButtonArea';
import CircleMinusButton from '../buttons/CircleMinusButton';
import FAB from '../buttons/FAB';
import Checkbox from '../Checkbox/Checkbox';
import Modal from '../Modal/Modal';
import Separator from '../Separator/Separator';
import AddTodoInput from '../todos/AddTodoInput';
import EditTodoInput from '../todos/EditTodoInput';
import TodoItemContainer from '../todos/TodoItemContainer';
import TodoItemText from '../todos/TodoItemText';
import TodosContainer from '../todos/TodosContainer';

const Header = styled.h1`
  font-family: Gilroy;
  font-style: normal;
  font-weight: 800;
  font-size: 1.8rem;
  line-height: 2.7rem;
  letter-spacing: 0.01em;
  color: #1C1C1C;
`

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
`
const MainInner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 90%;
  width: 80%;
`

function App({ todos, addTodo, deleteTodo, updateTodo }) {
  const [isAddingTodoModalOpen, setAddingTodoModalOpen] = useState(false)
  const [isEditingTodoModalOpen, setEditingTodoModalOpen] = useState(false)
  const [isTodosEditing, setTodosEditing] = useState(false)
  const [todoText, setTodoText] = useState('')
  const [selectedTodo, setSelectedTodo] = useState<App.ITodo>({} as App.ITodo)

  return (
    <MainContainer onClick={() => {
      isAddingTodoModalOpen ? setAddingTodoModalOpen(false) : null;
      isEditingTodoModalOpen ? setEditingTodoModalOpen(false) : null;
    }}>
      <MainInner>
        <ButtonArea>
          <Header> Сегодня </Header>
          {
            isTodosEditing ?
              <Button size="small" onClick={() => setTodosEditing(false)}> Отменить </Button>
              : <Button size="small" onClick={() => setTodosEditing(true)}> Править </Button>
          }
        </ButtonArea>
        <TodosContainer>
          {todos.length ? todos.map((todo: App.ITodo) => (
            <TodoItemContainer key={todo.uid}>
              {
                isTodosEditing ?
                  <CircleMinusButton onClick={() => deleteTodo(todo.uid)} />
                  : <Checkbox checked={todo.checked} onChange={(event) => updateTodo({ ...todo, checked: !todo.checked })} />
              }
              <Separator width='1rem' />
              <TodoItemText checked={todo.checked} onClick={() => isTodosEditing ? (setEditingTodoModalOpen(true), setSelectedTodo(todo)) : (updateTodo({ ...todo, checked: !todo.checked }))}>
                {todo.name}
              </TodoItemText>
            </TodoItemContainer>
          )) : <TodoItemText> Список задач пуст </TodoItemText>}
        </TodosContainer>
        <ButtonArea justify="flex-end">
          {
            !isTodosEditing ?
              <FAB iconUrl='./images/plus.svg' onClick={() => setAddingTodoModalOpen(!isAddingTodoModalOpen)} />
              : null
          }
        </ButtonArea>
      </MainInner>

      <Modal isOpen={isAddingTodoModalOpen}>
        <AddTodoInput value={todoText} onChange={event => { setTodoText(event.target.value) }} placeholder="Введите текст задачи" />
        <ButtonArea>
          <Button onClick={() => setAddingTodoModalOpen(false)}> Закрыть </Button>
          <Button blue onClick={() => (addTodo(todoText), setAddingTodoModalOpen(false), setTodoText(''))}> Добавить </Button>
        </ButtonArea>
      </Modal>

      <Modal isOpen={isEditingTodoModalOpen}>
        <EditTodoInput value={selectedTodo.name} onChange={event => { setSelectedTodo({ ...selectedTodo, name: event.target.value }) }} placeholder="Введите текст задачи" />
        <ButtonArea>
          <Button onClick={() => (setEditingTodoModalOpen(false), setSelectedTodo({} as App.ITodo))}> Закрыть </Button>
          <Button blue onClick={() => (updateTodo(selectedTodo), setEditingTodoModalOpen(false), setSelectedTodo({} as App.ITodo))}> Сохранить </Button>
        </ButtonArea>
      </Modal>

    </MainContainer>
  );
}

const mapStateToProps = state => ({ ...state.todoList })

const mapDispatchToProps = dispatch => ({
  addTodo: (name: string) => dispatch(todoListAction.addTodo(name)),
  deleteTodo: (uid: string) => dispatch(todoListAction.deleteTodo(uid)),
  updateTodo: (todo: App.ITodo) => dispatch(todoListAction.updateTodo(todo)),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);