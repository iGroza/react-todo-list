import React from 'react'
import styled from 'styled-components'

const AddTodoInput = styled.textarea`
  width: 100%;
  height: 40%;
  box-sizing: border-box;
  resize: none;
  border: 2px solid #E6E6E6;
  box-sizing: border-box;
  border-radius: 10px;
  font-family: Gilroy;
  font-style: normal;
  font-weight: 500;
  font-size: 1.3rem;
  line-height: 140%;
  letter-spacing: 0.01rem;
  outline:none;
  color: #333333;
  padding: 1rem;
`

export default AddTodoInput
