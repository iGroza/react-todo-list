import React from 'react'
import styled from "styled-components";

const TodoItemContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 1rem 0;
`

export default TodoItemContainer