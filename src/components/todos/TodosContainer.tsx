import React from 'react'
import styled from 'styled-components'

const TodosContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  align-items: left;
  height: 100%
`

export default TodosContainer