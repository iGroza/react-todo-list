import React from 'react'
import styled from 'styled-components'
import AddTodoInput from './AddTodoInput'

const EditTodoInput = styled(AddTodoInput)`
  height: 15%;
`

export default EditTodoInput
