import React from 'react'
import styled from 'styled-components'

interface ITodoItemTextProps {
    checked?: boolean;
  }
  
  const TodoItemText = styled.div<ITodoItemTextProps>`
    font-family: Gilroy;
    font-style: normal;
    font-weight: 500;
    font-size: 1.2rem;
    line-height:1.4rem;
    letter-spacing: 0.01rem;
    color: ${props => props.checked ? "rgba(0, 0, 0, 0.6)" : "#000000"};
  `
  
  export default TodoItemText