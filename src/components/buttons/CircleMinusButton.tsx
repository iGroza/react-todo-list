import React from 'react'
import styled from 'styled-components'
import CircleButton from './CircleButton'

interface IMinusProps {
    minusColor?: string;
}

const Minus = styled.div<IMinusProps>`
    position: absolute;
    width: 60%;
    height: 0.2rem;
    border-radius: 10px;
    background: ${props => props.minusColor || '#FFFFFF'};
`

interface ICircleMinusButtonProps {
    color?: string
    minusColor?: string,
    onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent> )=> void
}

const CircleMinusButton = ({ color, minusColor, onClick }: ICircleMinusButtonProps) => (
    <CircleButton color={color} onClick={onClick}>
        <Minus minusColor={minusColor} />
    </CircleButton>
)

export default CircleMinusButton