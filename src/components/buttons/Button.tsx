import React from 'react'
import styled from 'styled-components'

interface IButtonProps {
  blue?: any
  size?: "small" | "large"
}

const Button = styled.div<IButtonProps>`
    background: ${(props: IButtonProps) => props && props.blue ? '#23A3FF' : '#F2F2F2'};
    color: ${(props: IButtonProps) => props && props.blue ? '#FFFFFF' : '#333333'};
    font-family: Gilroy;
    font-style: normal;
    font-weight: 500;
    font-size: ${props => {
      switch (props.size) {
        case "small": return "0.9rem";
        case "large": return '1.4rem';
        default: return '1.4rem'
      }
    }};
    line-height: 1.2rem;
    letter-spacing: 0.01em;
    height: ${props => {
    switch (props.size) {
      case "small": return "2rem";
      case "large": return '3.3rem';
      default: return '3.3rem'
    }
  }};
    width: ${props => {
    switch (props.size) {
      case "small": return "30%";
      case "large": return '45%';
      default: return '45%'
    }
  }};
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
  
    :active {
      opacity: 0.5;
    }
  `

export default Button