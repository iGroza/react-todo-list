import React from 'react'
import styled from 'styled-components'
import Button from './Button'

interface ICircleButton {
    color?: string;
}

const CircleButton = styled(Button) <ICircleButton>`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 1.4rem;
    height: 1.4rem;
    border-radius: 30px;
    background: ${props => props.color || "#E85C5C"};
`

export default CircleButton