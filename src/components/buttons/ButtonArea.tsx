import React from 'react'
import styled from 'styled-components'

interface IButtonAreaProps {
    justify?: string;
}
const ButtonArea = styled.div<IButtonAreaProps>`
    display: flex;
    justify-content: ${(props: IButtonAreaProps) => props.justify ? props.justify : "space-between"};
    align-items: center;
    height: 6rem;
 `

export default ButtonArea