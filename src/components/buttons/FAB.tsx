import React from 'react'
import styled from "styled-components";

interface IFABProps{
    iconUrl: string;
}

const FAB = styled.div<IFABProps>`
  width: 3rem;
  height: 3rem;
  background: #FFFFFF;
  background-image: url(${ props => props.iconUrl});
  background-repeat: no-repeat;
  box-shadow: 0px 10px 40px rgba(0,0,0,0.1);
  background-position: center;
  border-radius: 30px;
`

export default FAB