import React, { useEffect, useState } from 'react'
import styled from 'styled-components';

const Wrapper = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 1.4rem;
    height: 1.4rem;
`

const Input = styled.input<any>`
    opacity: 0;
    width: 100%;
    height: 100%;
    position: absolute;
`

const CustomInput = styled.div`
    border: 2px solid #D9D9D9;
    border-radius: 30px;
    width: 100%;
    height: 100%;
`

const CustomInputChecked = styled.div`
    border: 2px solid #D9D9D9;
    border-radius: 30px;
    width: 100%;
    height: 100%;
    background: rgb(35,163,255);
    background: radial-gradient(circle,rgba(35,163,255,1) 49%,rgba(35,163,255,1) 49%,rgba(255,255,255,1) 49%,rgba(255,255,255,1) 49%);`

interface ICheckboxProps {
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void,
    checked?: boolean;
}
const Checkbox = ({ onChange, checked: customChecked }: ICheckboxProps) => {
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        setChecked(customChecked)
    }, [customChecked])

    return (
        <Wrapper>
            <Input type="checkbox" checked={checked} onChange={event => (setChecked(event.target.checked), onChange(event))} />
            {checked ? <CustomInputChecked /> : <CustomInput />}
        </Wrapper>
    )
}

export default Checkbox