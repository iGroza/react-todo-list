import React from 'react';
import styled from 'styled-components';
import "./Modal.css"

const Modal = styled.div`
    width: 100%;
    height: 75%;
    display: flex;
    background: #ffffff;
    box-shadow: 0 0 4rem rgba(0, 0, 0, 0.1);
    border-radius: 4rem 4rem 0 0;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
`

const ModalInner = styled.div`
    width: 80%;
    height: 80%;
`

interface IModalProps {
    isOpen: boolean;
    children?: any;
}

export default ({ isOpen, children }: IModalProps) => {
    return (
        <Modal className={isOpen ? 'modal__effect_show' : 'modal__effect_hide'} onClick={event => event.stopPropagation()}>
            <ModalInner>
                {children}
            </ModalInner>
        </Modal>
    )
}