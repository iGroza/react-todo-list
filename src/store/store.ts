import { createStore } from 'redux'
import rootReducer from './application/rootReducer';

export const store = createStore( rootReducer, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());
