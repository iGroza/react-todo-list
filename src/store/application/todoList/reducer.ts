import { TODO_CONSTANTS } from './constants'

const initialState = {
    todos: new Array<App.ITodo>()
}

export const todoList = (state = initialState, action: Store.IBaseAction) => {
    switch (action.type) {
        case TODO_CONSTANTS.ADD: {
            const newTodos = [...state.todos, action.payload]
            return { ...state, todos: newTodos }
        }
        case TODO_CONSTANTS.DELETE: {
            const newTodos = state.todos.filter(todo => todo.uid !== action.payload.uid)
            return { ...state, todos: newTodos }
        }
        case TODO_CONSTANTS.UPDATE: {
            const newTodos = state.todos.map(todo => todo.uid === action.payload.uid ? action.payload : todo)
            return { ...state, todos: newTodos }
        }
        default: return state
    }
}
