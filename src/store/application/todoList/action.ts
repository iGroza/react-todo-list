import { v4 } from 'uuid'
import { TODO_CONSTANTS } from './constants'

const addTodo = (name: string): Store.IBaseAction<App.ITodo> => {
    return {
        type: TODO_CONSTANTS.ADD,
        payload: {
            name,
            uid: v4(),
            timestamp: Date.now(),
            checked: false
        }
    }
}

const deleteTodo = (uid: string): Store.IBaseAction<{ uid: string }> => {
    return {
        type: TODO_CONSTANTS.DELETE,
        payload: {
            uid
        }
    }
}

interface IUpdatedTodoProps {
    uid: string;
    name: string;
    checked: boolean;
}
const updateTodo = ({ checked = false, uid, name }: IUpdatedTodoProps): Store.IBaseAction<App.ITodo> => {
    return {
        type: TODO_CONSTANTS.UPDATE,
        payload: {
            name,
            uid,
            checked,
            timestamp: Date.now()
        }
    }
}

export const todoListAction = {
    addTodo,
    updateTodo,
    deleteTodo
}