declare module Store {
    interface IBaseAction<T = any> {
        type: string
        payload?: T;
    }
}

declare module App {
    interface ITodo{
        name: string;
        uid: string;
        timestamp: number;
        checked: boolean;
    }
}

